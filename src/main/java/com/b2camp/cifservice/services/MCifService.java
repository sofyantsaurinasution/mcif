package com.b2camp.cifservice.services;

import com.b2camp.cifservice.dtos.MCifRequest;
import com.b2camp.cifservice.dtos.MCifResponse;

import java.util.List;

public interface MCifService {

    MCifResponse addMcif (MCifRequest request);

    MCifResponse updateMcif (String id, MCifRequest request);

    List<MCifResponse> showUpMcif();

    void deleteData (String id);
}
