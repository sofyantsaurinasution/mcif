package com.b2camp.cifservice.services;

import com.b2camp.cifservice.dtos.MCifRequest;
import com.b2camp.cifservice.dtos.MCifResponse;
import com.b2camp.cifservice.models.MCif;
import com.b2camp.cifservice.repositories.MCifRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MCifServiceImpl implements  MCifService{

    private MCifRepository cifRepository;

    @SneakyThrows
    @Transactional
    @Override
    public MCifResponse addMcif(MCifRequest request) {
        MCif requestData = MCif.builder()
                .idKtp(request.getIdKtp())
                .name(request.getName())
                .email(request.getEmail())
                .noTelephone(request.getNoTelephone())
                .npwp(request.getNpwp())
                .type(request.getType())
                .createdBy("SOFYAN")
                .createdDate(LocalDate.now())
                .isDeleted(false)
                .build();

        MCif save = cifRepository.save(requestData);

        return MCifResponse.builder()
                .id(save.getId())
                .idKtp(save.getIdKtp())
                .name(save.getName())
                .email(save.getEmail())
                .noTelephone(save.getNoTelephone())
                .npwp(save.getNpwp())
                .type(save.getType())
                .createdDate(save.getCreatedDate())
                .createdBy(save.getCreatedBy())
                .build();
    }

    @SneakyThrows
    @Transactional
    @Override
    public MCifResponse updateMcif(String id, MCifRequest request){
        Optional<MCif> cariId = cifRepository.findById(id);
        if(cariId.isEmpty()){
            throw  new Exception("data tidak ada");
        }
        cariId.get().setIdKtp(request.getIdKtp());
        cariId.get().setName(request.getName());
        cariId.get().setEmail(request.getEmail());
        cariId.get().setNoTelephone(request.getNoTelephone());
        cariId.get().setNpwp(request.getNpwp());
        cariId.get().setType(request.getType());

        MCif saved = cifRepository.save(cariId.get());

        return MCifResponse.builder()
                .id(saved.getId())
                .idKtp(saved.getIdKtp())
                .name(saved.getName())
                .npwp(saved.getNpwp())
                .noTelephone(saved.getNoTelephone())
                .type(saved.getType())
                .email(saved.getEmail())
                .createdBy(saved.getCreatedBy())
                .createdDate(saved.getCreatedDate())
                .build();
    }

    @SneakyThrows
    @Override
    public List<MCifResponse> showUpMcif() {
        List<MCif> cariId = cifRepository.findAll();

        List<MCifResponse> responses = new ArrayList<>();

        cariId.forEach(x -> {
            MCifResponse response = MCifResponse.builder()
                    .id(x.getId())
                    .idKtp(x.getIdKtp())
                    .name(x.getName())
                    .email(x.getEmail())
                    .noTelephone(x.getNoTelephone())
                    .npwp(x.getNpwp())
                    .type(x.getType())
                    .createdDate(x.getCreatedDate())
                    .createdBy(x.getCreatedBy())
                    .build();

            responses.add(response);
        });
        return responses;
    }

    @SneakyThrows
    @Transactional
    @Override
    public void deleteData(String id) {
        Optional<MCif> deleteResponse = cifRepository.findById(id);
        if(deleteResponse.isEmpty()){
            throw new Exception("data tidak ada");
        }
        cifRepository.deleteById(id);
    }
}
