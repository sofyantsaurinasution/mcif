package com.b2camp.cifservice.repositories;

import com.b2camp.cifservice.models.MCif;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MCifRepository extends JpaRepository<MCif, String> {
}
