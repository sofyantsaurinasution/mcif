package com.b2camp.cifservice.controller;

import com.b2camp.cifservice.dtos.MCifRequest;
import com.b2camp.cifservice.dtos.MCifResponse;
import com.b2camp.cifservice.services.MCifService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/cif")
@AllArgsConstructor
public class MCifController {

    private MCifService cifService;

    @PostMapping
    public MCifResponse addMci(@RequestBody MCifRequest request){
        return  cifService.addMcif(request);
    }

    @PutMapping(value = "/{id}")
    public MCifResponse updateMci(@PathVariable("id") String id, @RequestBody MCifRequest request ){
        return cifService.updateMcif(id, request);
    }

    @GetMapping
    public List<MCifResponse> showMciList(){
        return cifService.showUpMcif();
    }

    @DeleteMapping(value = "/{id}")
    public String deleteMcif(@PathVariable("id") String id){
        cifService.deleteData(id);
        return "delete data berhasil";
    }
}
