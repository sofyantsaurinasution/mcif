package com.b2camp.cifservice.dtos;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
public class MCifResponse {

    private String id;
    private String idKtp;
    private String name;
    private String npwp;
    private String noTelephone;
    private String email;
    private String type;

    private String createdBy;
    private LocalDate createdDate;
}
