package com.b2camp.cifservice.models;

import com.b2camp.cifservice.enums.MTypeCIF;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "m_cif")
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class MCif extends BaseEntity{

    @Id
    @GeneratedValue(generator = "cif-system")
    @GenericGenerator(name ="cif-system", strategy = "uuid2")
    @Column(name = "id", nullable = false, length = 64)
    private String id;

    @Column(name = "name", nullable = false, length = 20)
    private String name;

    @Column(name = "email", length = 64)
    private String email;

    @Column(name = "id_ktp", unique = true, nullable = false, length = 64)
    private String idKtp;

    @NotEmpty(message = "data tidak boleh kosong")
    @Column(name = "no_telephone", unique = true, nullable = false, length = 64)
    private String noTelephone;

    @Column(name = "npwp", unique = true, length = 64)
    private String npwp;

    @Column(name = "type", nullable = false , length = 25)
    private String type;
}
