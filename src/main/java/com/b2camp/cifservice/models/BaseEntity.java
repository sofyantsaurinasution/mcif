package com.b2camp.cifservice.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class BaseEntity {

    @Column ( name = "created_by", length = 20 )
    private String createdBy;

    @CreationTimestamp
    @Column ( name = "created_date", columnDefinition = "DATE")
    private LocalDate createdDate;

    @Column ( name = "updated_by", length = 20 )
    private String updatedBy;

    @UpdateTimestamp
    private LocalDateTime updatedDate;

    @Column ( name = "is_deleted" )
    private Boolean isDeleted = Boolean.FALSE;
}
